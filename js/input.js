(function($){

	
	/*
	*  acf/setup_fields
	*
	*  This event is triggered when ACF adds any new elements to the DOM. 
	*
	*  @type	function
	*  @since	1.0.0
	*  @date	01/01/12
	*
	*  @param	event		e: an event object. This can be ignored
	*  @param	Element		postbox: An element which contains the new HTML
	*
	*  @return	N/A
	*/

	$(document).live('acf/setup_fields', function(e, postbox){

		$('.acf_vimeo_delete a').on('click', function(e){
			e.preventDefault();

			var $this = $(this);

			$.ajax({
				url:      acf_vimeo.url,
				type:     'post',
				async:    true,
				cache:    false,
				dataType: 'html',
				data: {
					action:   'acf_vimeo_delete',
					post_id:  acf_vimeo.multipart_params.post_id,
					key:      $this.data('key'),
					nonce:    $this.data('nonce')
				},

				success: function( response )
				{
					console.log(response);
					if ( '1' === response )
					{
						var $video_container  = $this.closest('#acf-field-video_container'),
							$upload_container = $('.acf_vimeo_upload', $video_container),
							$field_container  = $this.closest('#field_type-vimeo_uploader');

						// Empty the input, so it doesn't come back
						$('input[data-key="'+$this.data('key')+'"').val('');

						// Physically remove items from page
						$upload_container.fadeOut(400, function(){
							$(this).removeClass('acf_vimeo_upload_show');
							$('.acf_vimeo_video, p.vimeo_uploader_success', $upload_container).remove();
						});
						$('#acf-field-video_browse', $video_container).removeClass('vimeo_uploader_button_hide').fadeIn();
					}
				},

				error: function( xhr )
				{
					console.log(xhr.responseText);
				}
			});
		});

		$(postbox).find('.vimeo_uploader').each(function(){

			var $this   = $(this),
				this_id = $this.attr('id'),
				key     = $this.data('key'),
				$form   = $this.closest('form');

			acf_vimeo.browse_button  = this_id + acf_vimeo.browse_button;
			acf_vimeo.container      = this_id + acf_vimeo.container;
			acf_vimeo.drop_element   = this_id + acf_vimeo.drop_element;
			acf_vimeo.file_data_name = key + acf_vimeo.file_data_name;

			acf_vimeo.multipart_params.key = key;

			var container_id = acf_vimeo.container;
			var $container = $('#' + container_id);
			var $filelist = $container.find('.acf_vimeo_filelist');

			var uploader = new plupload.Uploader(acf_vimeo);
			uploader.init();

			// Add file to the queue and for visual feedback
			uploader.bind('FilesAdded', function(up, files) {

				$('button, input[type="submit"], input[type="button"]', $form).prop('disabled', true).addClass('disabled').css({opacity: '0.6'});

				// Hide the upload button
				$('#acf-field-video_browse', $container).fadeOut('400', function(){ $(this).addClass('vimeo_uploader_button_hide'); });

				$.each(files, function(i, file) {
					$filelist.append(
						'<div class="file" id="' + file.id + '">' +
						'<strong>' + file.name + '</strong>' +
						' (<span>' + plupload.formatSize(0) + '</span>/' + plupload.formatSize(file.size) + ') ' +
						'<div class="fileprogress"></div>' +
						'</div>'
					);
				});

				up.refresh();
				up.start();
			});

			// Update the progress bar for the uploaded file in the queue
			uploader.bind('UploadProgress', function(up, file) {
				$('#' + file.id + " .fileprogress").width(file.percent + "%");
				$('#' + file.id + " span").html( plupload.formatSize(parseInt(file.size * file.percent / 100)) );

				if ( 100 == file.percent && ! $('.acf_vimeo_spinner').length )
				{
					$filelist.append('<div class="acf_vimeo_spinner"><img src="' + acf_vimeo.multipart_params.spinner + '" /> Updating, Don\'t Refresh</div>');
				}
			});

			// Finished uploading
			uploader.bind('FileUploaded', function(up, file, response) {
				$('button, input[type="submit"], input[type="button"]', $form).prop('disabled', false).removeClass('disabled').css({opacity: '1'});

				console.log(response);
				if ( response.status )
				{
					response = $.parseJSON(response.response);
					console.log(response);

					$('.acf_vimeo_spinner, #' + file.id).fadeOut();

					// Add the video id to input
					$('#' + this_id).val(response.video_id);

					// Add the video thumbnail to the page
					var $upload_container = $('.acf_vimeo_upload', $container);
					$upload_container.fadeIn(400, function(){ $(this).addClass('acf_vimeo_upload_show'); }).prepend(response.embed).prepend('<p class="vimeo_uploader_success"><strong>Success!</strong></p>');

					$('#' + file.id).remove();
				}
			});

		});

	});

})(jQuery);
