<?php
/*
Plugin Name: Advanced Custom Fields: Vimeo Uploader
Plugin URI: {{git_url}}
Description: Allows you to upload to a single vimeo account that has upload API access.
Version: 1.0.4
Author: Creative Slice
Author URI: http://creativeslice.com/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


class acf_field_vimeo_uploader_plugin
{
	/*
	*  Construct
	*
	*  @description: 
	*  @since: 3.6
	*  @created: 1/04/13
	*/
	
	function __construct()
	{
		// set text domain
		/*
		$domain = 'acf-vimeo_uploader';
		$mofile = trailingslashit(dirname(__File__)) . 'lang/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		*/
		
		
		// version 4+
		add_action('acf/register_fields', array($this, 'register_fields'));	

		
		// version 3-
		add_action( 'init', array( $this, 'init' ), 5);
	}
	
	
	/*
	*  Init
	*
	*  @description: 
	*  @since: 3.6
	*  @created: 1/04/13
	*/
	
	function init()
	{
		if(function_exists('register_field'))
		{ 
			register_field('acf_field_vimeo_uploader', dirname(__File__) . '/vimeo_uploader-v3.php');
		}
	}
	
	/*
	*  register_fields
	*
	*  @description: 
	*  @since: 3.6
	*  @created: 1/04/13
	*/
	
	function register_fields()
	{
		include_once('vimeo_uploader-v4.php');
	}
	
}

new acf_field_vimeo_uploader_plugin();
		
?>
